/***************************************************************************
                          logotipe.h  -  description
                             -------------------
    begin                : Sun Oct 17 1999
    copyright            : (C) 1999 by Guillermo P. Marotte
    email                : g-marotte@usa.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef LOGOTIPE_H
#define LOGOTIPE_H

/* Library Includes */
#include <qwidget.h>

class logotipe : public QWidget
{
	 Q_OBJECT
public:
	logotipe(QWidget *parent, const char *name=0);
signals:
        /**
        * This signal is emitted when the user clicks into the widget.
        */
        void mouseClick( QMouseEvent *);
protected:
        virtual void mousePressEvent( QMouseEvent *);
};
#endif
