#ifndef AKTIONCONF
#define AKTIONCONF

#include <qimage.h>
#include <qslider.h>
#include <qchkbox.h>
#include <qradiobt.h>
#include <qlined.h>
#include <qlabel.h>
#include <qpushbt.h>
#include <qstrlist.h>
#include <qtabdlg.h>
#include <qtooltip.h>
#include <qgrpbox.h>
#include <qbttngrp.h>
#include <qcombo.h>
#include <qlistbox.h>
#include <qlayout.h>
#include <kspinbox.h>
#include <kfiledialog.h>
#include <kiconloader.h>
#include <kapp.h>

class Setup : public QTabDialog
{
    Q_OBJECT
public:
    Setup( QWidget *parent=0, const char *name=0 );
    ~Setup();
private:
    QLineEdit *lined[6];
    QCheckBox *checkBox[11];
    QRadioButton *botonRadio[13];
    KConfig *config;
    KIconLoader *loader;
    /* the tabs' constructors */
    QWidget *audio();
    QWidget *color();
    QWidget *scaling();
    QWidget *interface();
    QWidget *capture();
    QWidget *others();

    QLabel *label[12];
    QGroupBox *grupo[1];
    QButtonGroup *botones[4];
    QComboBox *combo[2];
    QListBox *lista[1];
    QPushButton *dirButton[2];
    QPushButton *executableButton;
    KNumericSpinBox *spin[1];
private slots:
    void quit();
    void save();
    void getDir();
    void getCaptureDir();
    void getXanimExecutable();
    void actualizeDialog(int);
    void actualizeDialog1(int);
    void actualizePixmap(int);
};
#endif
