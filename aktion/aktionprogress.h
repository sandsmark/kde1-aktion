/***************************************************************************
                          aktionprogress.h  -  description
                             -------------------
    begin                : Sun Jan 16 2000
    copyright            : (C) 2000 by Guillermo P. Marotte
    email                : g-marotte@usa.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef AKTIONPROGRESS_H
#define AKTIONPROGRESS_H

#include "kxanim.h"
#include <qlabel.h>
#include <qprogressbar.h>
#include <qregexp.h>
#include <qstring.h>
#include <qtimer.h>

#define TIMER_INTERVAL 250
/**
  *@author Guillermo P. Marotte
  */

class aktionProgress : public QProgressBar  {
	Q_OBJECT
public: 
	aktionProgress(QWidget *, const char *, WFlags);
	~aktionProgress();
  /** You need to pass the KXAnim Object */
  void setParameters(KXAnim *,QLabel *, QLabel *);
private:
	long int frames;
	float fps;
  /** Timer to update the values */
  QTimer timer;
  /** Reference to the video object from which
	to take the frames information */
  KXAnim *video;
  /**  */
  QLabel * elapsedLabel;
  /**  */
  /**  */
  QString aux;
  QLabel * totalLabel;
public slots: // Public slots
  void play();
  /**  */
  void stepbw();
  /**  */
  void stepfw();
  /**  */
  void stop();
  /**  */
  void pause();
  /**  */
private slots: // Private slots
  /** Update the frame counter */
  void update();

protected: // Protected methods
  /** Show the time indicator */
  virtual bool setIndicator( QString &, int, int);
};

#endif
























