#include <iostream>
#include <qdir.h>

#ifdef XF86_VM
#include <unistd.h>
#endif

#include "principal.h"
using std::cout;

principal::principal( char *)
                            // : QDialog(0L,"dialog",FALSE,0)
{
	setCaption("aKtion!");

	config=kapp->getConfig();

	loader = kapp->getIconLoader();
	abrir = new KButton(this, "abrir");
	abrir->setPixmap(loader->loadIcon("fileopen.xpm"));
	QToolTip::add(abrir,i18n("Open file"));
	
	tocar = new KButton(this, "tocar");
	tocar->setPixmap(loader->loadIcon("tocar.xpm"));
	QToolTip::add(tocar,i18n("Play"));
	
	parar = new KButton(this, "parar");
	parar->setPixmap(loader->loadIcon("parar.xpm"));
	QToolTip::add(parar,i18n("Stop"));

	avanzar = new KButton(this, "avanzar");
	avanzar->setPixmap(loader->loadIcon("avanzar.xpm"));
        avanzar->setAutoRepeat( true );
	QToolTip::add(avanzar,i18n("Forward"));

	retroceder = new KButton(this, "retroceder");
	retroceder->setPixmap(loader->loadIcon("retroceder.xpm"));
	retroceder->setAutoRepeat( true );
	QToolTip::add(retroceder,i18n("Backward"));

  custom = new KButton(this, "custom");
  config->setGroup("interface");
  QToolTip::add(custom,"?");
  setCustomButton(config->readNumEntry("custombutton"));

	configB = new KButton(this, "Setup");
	configB->setPixmap(loader->loadIcon("ak_setup.xpm"));
	QToolTip::add(configB,i18n("Setup"));

	volumeSlider = new QSlider(this);
	volumeSlider->hide();
	volumeSlider->setOrientation(QSlider::Horizontal);
	volumeSlider->setRange(0,100);
	volumeSlider->setSteps(1,20);
	volumeSlider->setTickmarks( QSlider::Right );
	volumeSlider->setTickInterval(20);
	volumeSlider->setTracking(true);
	QToolTip::add(volumeSlider,i18n("Volume"));
	config->setGroup("audio");
  volumeSlider->setValue(config->readNumEntry("audioInitialVolume"));

	line = new QFrame(this, "separator");
  line->setFrameStyle( QFrame::HLine | QFrame::Sunken );

  timer = new QTimer(this, "timer");

	timeBar = new aktionProgress(this, "progress", 0);
	QToolTip::add(timeBar,i18n("Elapsed time"));

	QFont f("Helvetica", 10);
	totalTime = new QLabel(this);
	totalTime->setFont(f);
	totalTime->setText("0:00:00");
	QToolTip::add(totalTime,i18n("Total time"));
	elapsedTime = new QLabel(this);
	elapsedTime->setFont(f);
	elapsedTime->setText("0:00:00");
	QToolTip::add(elapsedTime,i18n("Elapsed time"));

	menu = new QPopupMenu(0L, "menu");
	menu->insertItem(i18n("Video information"),this,SLOT(click_info()) );
	menu->insertSeparator();
	menu->insertItem(loader->loadIcon("fileopen.xpm"),
	                 i18n("Open file..."),this,SLOT(click_open()) );
	menu->insertItem(loader->loadIcon("ak_setup.xpm"),
	                 i18n("Setup..."),this,SLOT(click_config()) );
	menu->insertItem(loader->loadIcon("ayuda.xpm"),
	                 i18n("Help"),this,SLOT(click_ayuda()) );
  menu->insertSeparator();
  menu->insertItem(loader->loadIcon("original.xpm"),
                   i18n("Original size"),this,SLOT(click_original()) );
  menu->insertItem(loader->loadIcon("half.xpm"),
                   i18n("Half size"),this,SLOT(click_half()) );
  menu->insertItem(loader->loadIcon("double.xpm"),
                   i18n("Double size"),this,SLOT(click_double()) );
  menu->insertItem(loader->loadIcon("ak_maximize.xpm"),
                   i18n("Maximized"),this,SLOT(maximize()) );
  menuItemFullScreen = menu->insertItem(loader->loadIcon("fullscreen.xpm"),
                   i18n("Full screen"),this,SLOT(goFullScreen()) );
	menu->insertSeparator();
	menu->insertItem(loader->loadIcon("exit.xpm"),
	                 i18n("Exit"),this,SLOT(click_exit()) );

  initialMenu = new QPopupMenu(0L, "initialMenu");
  initialMenu->insertItem(loader->loadIcon("fileopen.xpm"),
                    i18n("Open file..."),this,SLOT(click_open()) );
	initialMenu->insertItem(loader->loadIcon("ak_setup.xpm"),
	                        i18n("Setup..."),this,SLOT(click_config()) );
	initialMenu->insertItem(loader->loadIcon("ayuda.xpm"),
	                        i18n("Help"),this,SLOT(click_ayuda()) );
  initialMenu->insertSeparator();
	initialMenu->insertItem(loader->loadIcon("exit.xpm"),
	                        i18n("Exit"),this,SLOT(click_exit()) );

	dropZone = new KDNDDropZone( this, DndURL);

#ifdef XF86_VM
        videoMode=new aktionVm(this);
        inVmMode=false;
#endif

  video = new KXAnim(this, "video");
  video->move(0,0);
  video->hide();
  video->setAutoResize(false);

  connect(abrir,SIGNAL(clicked()),
				  this,SLOT(click_open()) );
  connect(configB,SIGNAL(clicked()),
					this,SLOT(click_config()) );
  connect(tocar,SIGNAL(clicked()),
				  this,SLOT(click_play()) );
  connect(parar,SIGNAL(clicked()),
				  this,SLOT(click_stop()));
  connect(custom,SIGNAL(clicked()),
				  this,SLOT(click_custom()) );
  connect(avanzar,SIGNAL(clicked()),
				  this,SLOT(click_forward()));
  connect(retroceder,SIGNAL(clicked()),
				  this,SLOT(click_backward()));
  connect(video,SIGNAL(stopped()),
          this,SLOT(waitForKXanimExit()) );
  connect(video,SIGNAL(mouseClick(QMouseEvent *)),
          this,SLOT(receiveMouseClick(QMouseEvent *)) );
  connect(volumeSlider,SIGNAL(valueChanged(int)),
          this,SLOT(volumeSliderMoved(int)) );
  connect(timer,SIGNAL(timeout()),
          this,SLOT(checkStates()) );

  connect(dropZone,SIGNAL(dropAction(KDNDDropZone *)),
				  this,SLOT(fileDroped(KDNDDropZone *)));

  /* set the window's minimum size */
  selectMinimumSize();

  /* set the logo */
  config->setGroup("interface");
  if (config->readBoolEntry("showBars")==true)
	   resize(205,177);
	else
     resize(205,155);

  logoWidget = new logotipe( this, "logo");
  logoWidget->setGeometry(0,0,205,122);
  logoWidget->setBackgroundPixmap( loader->loadIcon("aktion.xpm") );
  connect(logoWidget,SIGNAL(mouseClick(QMouseEvent *)),
           this,SLOT(receiveMouseClick(QMouseEvent *)) );

  config->setGroup("others");
  lastDir=config->readEntry("initialDirectory");
  if (!QDir(lastDir).exists())
  {
    QMessageBox::information(0L,i18n("aktion error!"),
                              i18n("Invalid initial directory"));
		lastDir="";
  }

  /* switch the buttons to the 'disabled' mode */
  toggleButtons(false);

  whatToDo=NOTHING;
  parametersChanged=false;
  isNewVideo=false;
  fileName="";
  externalChange=false;
  inFullScreen=false;
}

principal::~principal()
{
}

void principal::toggleButtons(bool state)
{
   tocar->setEnabled(state);
   parar->setEnabled(state);
   avanzar->setEnabled(state);
   retroceder->setEnabled(state);
}

void principal::click_original() { changeSize(1.0); };
void principal::click_half()     { changeSize(0.5); };
void principal::click_double()   { changeSize(2.0); };

void principal::changeSize(float zoom)
{
   /* resize the window according to a zoom factor */
   int h,w;
   float zH, zW;

   if (inFullScreen)
      undoFullScreen();

   selectMinimumSize();

   zH=(float)video->getVideoHeight()*zoom;
   zW=(float)video->getVideoWidth()*zoom;
   config->setGroup("interface");
   if (config->readBoolEntry("showBars")==true)
   {
      //line   slider   buttons
      h=  2  +   28   +   25    + (int)zH;
   }
   else
      h=2+6+25+(int)zH;

   if (zW<(25*7+15))
      w=25*7+15;
   else
      w=(int)zW;
   
   resize(w,h);
}

void principal::maximize()
{
   /* maximizes the window */
   QRect with_frame, no_frame, result;

   selectMinimumSize();

   with_frame = KWM::geometry(winId(),true);
   no_frame   = KWM::geometry(winId(),false);
   result     = KWM::getWindowRegion( KWM::currentDesktop() );

   result.setWidth( result.width() - (with_frame.width() - no_frame.width()) );
   result.setHeight( result.height() - (with_frame.height() - no_frame.height()) );
   setGeometry( result );
}

void principal::changeInitialSize()
{
   /* this procedure sets the window size just after a video
      has been opened */
   config->setGroup("scaling");
   switch (config->readNumEntry("scale"))
   {
      case 0: changeSize(1.0); break;
      case 1: changeSize(0.5); break;
      case 2: changeSize(2.0); break;
      case 3: maximize(); break;
      case 4: goFullScreen(); break;
   }
}

void principal::dynamicResize()
{
   /* This method is called when the user has selected a config option that
      affects the window size (by now, only showing/hinding the volume
      slider and time bar)
   */
   config->setGroup("interface");
   if (config->readBoolEntry("showBars")==false && volumeSlider->isVisible()==true)
   {
      resize(width(),height()-22);
   }
   if (config->readBoolEntry("showBars")==true && volumeSlider->isVisible()==false)
   {
      resize(width(),height()+22);
   }
}

void principal::selectMinimumSize()
{
   /* Choses the right minimum size (having in mind the vol. slider */
   int minH=0;

   config->setGroup("interface");
   if (config->readBoolEntry("showBars")==true)
   {
      //   line   slider   buttons
      minH=  2  +   28   +   25;
   }
   else
      minH=2+6+25;

   setMinimumSize(25*7+15,minH+1);
}

void principal::waitForKXanimExit()
{
    /* the kxanim's stopped signal activates this slot every time the video
       is stopped. I use the whatToDo switch to know "what to do now that
       kxanim is stopped". This is the most secure way to manage the things,
       but not the cleaner, though.
    */
    tocar->setPixmap(loader->loadIcon("tocar.xpm"));
		timeBar->stop();
    switch (whatToDo)
    {
        case NOTHING          : if (inFullScreen)
                                {
                                   undoFullScreen();
                                   KWM::setGeometry( winId(), oldGeometry);
                                };
                                break;
        case CLOSE_THE_APP    : this->close(); break;
        case NEW_VIDEO        : click_open(); break;
        case CONTINUE_LOADING : continueLoading(); break;
        case CHANGE_EXECUTABLE: executableChanged(); break;
    }
}

void principal::closeEvent( QCloseEvent *e)
{
        if (video->isActive())
        {
            whatToDo=CLOSE_THE_APP;
            video->stop();
            e->ignore();
        }
        else
        {
            if (inFullScreen)
               undoFullScreen();
            kapp->quit();
            e->accept();
        }
}

void principal::click_open()
{
    /* the idea behind this method is not very simple:
       if fileName is empty then: get a file name!
       if the video is active (here comes the hard part!):
           set 'what to do' to NEW_VIDEO, stop the video and wait that the
           waitForKXanimExit slot gets activated...
       else
           ok, now that the video is REALLY inactive, we can play with it...!
           set the video file and wait until KXAnim finishes reading info...
           (continues on continueLoading() )
    */

    if ( fileName.isEmpty() )
        fileName=KFileDialog::getOpenFileName(lastDir,"*.avi *.mov *.mpg *.flc *.fli|All video formats\n"
        					      "*.avi|AVI files (*.avi)\n"
        					      "*.mov|QuickTime files (*.mov)\n"
        					      "*.mpg|MPG Files (*.mpg)\n"
        					      "*.fli *.flc|FLI/FLC Files (*.fli *.flc)\n"
        					      "*|All Files (*)\n"
        				      ,0L,"abrir");
    if (video->isActive() && !fileName.isEmpty() )
    {
        whatToDo=NEW_VIDEO;
        video->stop();
    }
    else
        if (!fileName.isEmpty())
        {
            whatToDo=CONTINUE_LOADING;
            kapp->setOverrideCursor( waitCursor );
            setParameters();
            parametersChanged=false;
            video->setFile(fileName);
        }
}

void principal::continueLoading()
{
   QString title;
   int pos;

   whatToDo=NOTHING;
   if (video->getErrorCode()==0)
   {
       /* Everything seems to be fine... */
       /* enable the buttons */
       toggleButtons(true);
       /* remove the logo picture */
       if (logoWidget != 0L)
       {
          delete logoWidget;
          logoWidget = 0L;
          video->show();
       }
       pos=fileName.findRev('/');
       title=fileName.mid(pos+1,fileName.length()-(pos+1));
       // title+=" - aKtion!";
       setCaption( title );
       lastDir=fileName.mid(0,pos+1);
       isNewVideo=true;
   
       theCapturer.resetCounter();
       config->setGroup("capture");
       theCapturer.setParameters(config->readEntry("outputDir"),fileName,
                                 config->readEntry("outputFormat"));

			 timeBar->setParameters(video, elapsedTime, totalTime);

       kapp->restoreOverrideCursor();
       changeInitialSize();
       click_play();
   }
   else
   {
       /* something is wrong... */
       /* disable buttons */
       toggleButtons(false);
       kapp->restoreOverrideCursor();
       QMessageBox::information(0L,i18n("aktion error!"),
                                video->getErrorString() );
   }
   fileName="";
}

void principal::fileDroped(KDNDDropZone * zone)
{
   QStrList myUrlList = zone->getURLList();
   KURL *myUrl = new KURL(myUrlList.first());
   if (myUrl->isLocalFile())
   {
	fileName=myUrl->path();
        click_open();
   }
}

void principal::click_ayuda()
{
    KApplication::getKApplication()->invokeHTMLHelp("aktion/aktion.html","");
}

void principal::click_play()
{
    /* maybe do we need to resize the whole application? */
    if (parametersChanged==true && video->isActive()==false)
    {
       setParameters();
       dynamicResize();
       parametersChanged=false;
    }
    /* autoplay or not */
    config->setGroup("others");
    if (isNewVideo==true && config->readBoolEntry("autoplay")==false)
    {
       video->setPauseAt(0);
    }
    else video->setPauseAt(-1);
    isNewVideo=false;

    /* set the play button pixmap and start/stop the timeBar */
    if (video->isPlaying()==true)
		{
      tocar->setPixmap(loader->loadIcon("tocar.xpm"));
			timeBar->pause();
		}
    else
		{
      tocar->setPixmap(loader->loadIcon("pause.xpm"));
			timeBar->play();
		}
/*
    static int ac = 0;
    if (ac) QColor::destroyAllocContext(ac);
    ac = QColor::enterAllocContext();

    int *numero;
    color = XListInstalledColormaps(x11Display(),video->winId(),numero);
    XUninstallColormap(x11Display(),color[0]);*/

    video->play();
//    temporizador.singleShot(3000,this, SLOT(mostrarColormaps() ));
}

void principal::mostrarColormaps()
{
    int *numero;
    color = XListInstalledColormaps(x11Display(),video->winId(),numero);
    cout<<"Colormaps instalados: "<<*numero<<" para la ventana: "<<video->winId()<<endl;
    XSetWindowColormap(x11Display(),winId(),color[0]);
    XInstallColormap(x11Display(),color[0]);
    XSync(x11Display(),false);
//    XFree( color );
}

void principal::click_forward()
{
   if (video->isPlaying()==true)
      tocar->setPixmap(loader->loadIcon("tocar.xpm"));
   video->stepForward();
	 timeBar->stepfw();
}

void principal::click_backward()
{
   if (video->isPlaying()==true)
      tocar->setPixmap(loader->loadIcon("tocar.xpm"));
   video->stepBack();
	 timeBar->stepbw();
}

void principal::click_stop()
{
   video->stop();
	 timeBar->stop();
}

void principal::click_config()
{
   QString oldExecutable, newExecutable;

   config->setGroup("others");
   oldExecutable=config->readEntry("executable");

   Setup dialogo(0L, "setup");
   if (dialogo.exec()==1)
   {
      parametersChanged=true;
      dynamicResize();

      config->setGroup("capture");
      theCapturer.setParameters(config->readEntry("outputDir"),fileName,
                                config->readEntry("outputFormat"));

      config->setGroup("interface");
      setCustomButton( config->readNumEntry("custombutton") );

      config->setGroup("others");
      newExecutable=config->readEntry("executable");
      if (oldExecutable!=newExecutable)
         executableChanged();
   }
}

void principal::setParameters()
{
    config->setGroup("audio");
    video->setAudio(config->readBoolEntry("enable"));
    video->setAudioSync(config->readBoolEntry("audioSync"));
    video->setInitialVolume(volumeSlider->value());

    config->setGroup("color");
    video->setColorAhead(config->readNumEntry("colorAhead"));
    switch (config->readNumEntry("colorMapping"))
    {
       case 0: video->setColorMapping(KXAnim::static332); break;
       case 1: video->setColorMapping(KXAnim::lookupTable); break;
       case 2: video->setColorMapping(KXAnim::grayScale); break;
       case 3: video->setColorMapping(KXAnim::none); break;
    }
    video->setGammaDisplay(float(config->readDoubleNumEntry("gammaDisp")));

    config->setGroup("scaling");
    /* I always wants "resinzing on the fly"

    video->setResizing(true);
    is not necessary 'cause it's true by default.
    */

//    video->setScaleFactor( float(config->readNumEntry("dispFactor"))/100 );
//    video->setScaleFactorB( float(config->readNumEntry("bufFactor"))/100);
    
    config->setGroup("others");
    video->setLoading(config->readNumEntry("loading"));
    video->setX11Shared(config->readBoolEntry("x11shared"));
    video->setMultiBuffer(config->readBoolEntry("multiBuffer"));
    video->setUsePixmap(config->readBoolEntry("pixmap"));
/*
    switch (config->readNumEntry("visualClass"))
    {
       case 0: video->setX11VisualClass("default"); break;
       case 1: video->setX11VisualClass("staticgray"); break;
       case 2: video->setX11VisualClass("grayscale"); break;
       case 3: video->setX11VisualClass("staticcolor"); break;
       case 4: video->setX11VisualClass("pseudocolor"); break;
       case 5: video->setX11VisualClass("truecolor"); break;
       case 6: video->setX11VisualClass("directcolor"); break;
    }
*/
    video->setLoop(config->readBoolEntry("loop"));
    video->setExtraParameters( config->readEntry("extras"));
    video->setExecutable( config->readEntry("executable") );
}

void principal::keyPressEvent( QKeyEvent *key)
{
   switch(key->key())
   {
      case Key_Space : click_play(); break;
      case Key_Period: click_forward(); break;
      case Key_Comma : click_backward(); break;
      case Key_Enter : click_open(); break;
      case Key_3     : if(volumeSlider->value() <= 99)
                       {
                          externalChange=true;
                          volumeSlider->setValue( volumeSlider->value() + 1);
                          externalChange=false;
                          video->volumeIncrement();
                       }
                       break;
      case Key_2     : if(volumeSlider->value() >= 1)
                       {
                          externalChange=true;
                          volumeSlider->setValue( volumeSlider->value() - 1);
                          externalChange=false;
                          video->volumeDecrement();
                       }
                       break;
      case Key_F1    : click_ayuda(); break;
      case Key_Escape: this->close(); break;
      case Key_C     : theCapturer.captureWidget(video); break;
      default        : if (inFullScreen)
                       {
                          undoFullScreen();
                          /* restore the geometry */
                          KWM::setGeometry( winId(), oldGeometry);
                       }
                       break;
   }
   key->accept();
}

void principal::receiveMouseClick( QMouseEvent *mouse)
{
   if (mouse->button()==RightButton)
   {
      if (logoWidget != 0L)
      {  /* show the -reduced- initial menu (Open/Config/Exit) */
         initialMenu->move(this->x()+mouse->x(),this->y()+mouse->y());
         initialMenu->show();
      }
      else
      {  /* show the normal popup menu */
         menu->move(this->x()+mouse->x(),this->y()+mouse->y());
         menu->show();
      }
   }
   else
   {
     if (mouse->button()==LeftButton && inFullScreen)
     {
        undoFullScreen();
        KWM::setGeometry( winId(), oldGeometry);
     }
   }
}

void principal::loadFile( const char *name)
{
   fileName=name;
   click_open();
}

void principal::click_info()
{
   QString s, codec;
   
   codec=video->getVideoCodec();
   s.sprintf("%s: %s\n"
             "%s\n"
             "%dx%d\n"
             "%d %s - %5.2f fps",
             i18n("File"), this->caption(),
             (const char *)codec,
             video->getVideoWidth(),video->getVideoHeight(),
             video->getVideoFrames(),i18n("frames"),video->getVideoSpeed());
   KMsgBox::message(this,"aktion!",s);
}

void principal::volumeSliderMoved(int v)
{
	if (!externalChange)
		video->setVolume(v);
}

void principal::resizeEvent( QResizeEvent *e)
{
   int w=e->size().width();
   int h=e->size().height();
   int extraH=0;
   float video_ar, space_ar, scaled;
   int videoW, videoH;

   if (inFullScreen==false)
   {
      /* calculate base height for aKtion controls
           buttons                             */
      h=h-   25;
      config->setGroup("interface");
      if (config->readBoolEntry("showBars")==true)
      {
				extraH=28;
   			volumeSlider->show();
				timeBar->show();
				totalTime->show();
				elapsedTime->show();
			}
			else
			{
				extraH=6;
				volumeSlider->hide();
				timeBar->hide();
				totalTime->hide();
				elapsedTime->hide();
			}

			abrir->setGeometry(2, h, 25,25);
			tocar->setGeometry(30, h, 25,25);
      parar->setGeometry(56, h, 25,25);
      retroceder->setGeometry(82, h, 25,25);
      avanzar->setGeometry(108, h, 25,25);
      custom->setGeometry(w - 26, h, 25,25);
      configB->setGeometry(w - 54, h, 25,25);
      h-=extraH;
			timeBar->setGeometry(2,h+5,w - 60,8);
			elapsedTime->setGeometry(2,h+5+10,35,10);
			totalTime->setGeometry(w-60-35,h+5+10,35,10);
      volumeSlider->setGeometry(w - 53, h+2,51,20);
      h-=2;
      line->setGeometry(0,h+2,w,2);
   }

   /* Here comes the resizing-moving process for the video widget.
      It'll maintain his aspect ratio. Steps are:
      .The available space for the video is defined w and h.
      .Resize the video widget as much as possible:
        .Calculate video and left space aspect ratio.
        .If video aspect ratio is smaller:
           expand in height
         else
           .If video aspect ratio is bigger
              expand in width
            else
              expand all
   */
   videoW=video->getVideoWidth();
   if (videoW==0) videoW=1;
   videoH=video->getVideoHeight();
   if (videoH==0) videoH=1;

   video_ar=(float)videoW/(float)videoH; // video aspect ratio
   space_ar=(float)w/(float)h;           // available space aspect ratio

   if (video_ar<space_ar)
   {
      scaled=(float)videoW*( (float)h/(float)videoH );
      video->resize( (int)scaled,h);
      scaled=((float)w - scaled) /2;
      video->move( (int)scaled, 0);

   }
   else
   {
      if (video_ar>space_ar)
      {
         scaled=(float)videoH*( (float)w/(float)videoW );
         video->resize(w, (int)scaled);
         scaled=((float)h - scaled) /2;
         video->move(0, (int)scaled);
      }
      else
      {
         video->resize(w,h);
         video->move(0,0);
      }
   }
}

void principal::checkStates()
{
//   cout<<video->iconText()<<endl;
}

void principal::goFullScreen()
{
   int sizeW=0, sizeH=0;

   /* change the popup menu */
   menu->setItemEnabled(menuItemFullScreen,false);

   /* Hide all widgets (except the video widget) */
   parar->hide();
   tocar->hide();
   abrir->hide();
   avanzar->hide();
   retroceder->hide();
   custom->hide();
   configB->hide();
   line->hide();
	 timeBar->hide();
	 totalTime->hide();
 	 elapsedTime->hide();
   volumeSlider->hide();

   /* Save the window's background color and geometry */
   saveBackground = backgroundColor();
   setBackgroundColor( black );
   oldGeometry = KWM::geometry( winId(), false);

#ifdef XF86_VM
   bool vmExtensionsOk=false;
   inVmMode=false;

   config->setGroup("scaling");
   if (config->readBoolEntry("usevm"))
   {
      if (true)//geteuid()==0)
      {
         sizeW=video->getVideoWidth();
         sizeH=video->getVideoHeight();
         vmExtensionsOk=videoMode->setVideoMode(&sizeW,&sizeH);
         if (vmExtensionsOk)
            inVmMode=true;
      }
      else
          QMessageBox::information(0L,i18n("aktion error!"),
                                   i18n("You must be root to use the XFree86-VidMode extensions\n"
                                        "Switching to normal full-screen..."));
   }

   if (!vmExtensionsOk)
   {
#endif
      /* Make the window FULL SCREEN in the traditional way */
      sizeW = QApplication::desktop()->width();
      sizeH = QApplication::desktop()->height();
#ifdef XF86_VM
   }
#endif

   inFullScreen = true;

   config->setGroup("scaling");
   if (config->readBoolEntry("grabmouse"))
   {
      kapp->setOverrideCursor( blankCursor );
      QCursor::setPos(0,0);
   }

   QPoint p;
   p.setX(0);
   p.setY(0);
   recreate(NULL, WStyle_Customize | WStyle_NoBorder ,
            p, true);

//   KWM::setDecoration( winId(), KWM::noDecoration | KWM::staysOnTop );
   setGeometry( 0, 0, sizeW, sizeH );
}

void principal::undoFullScreen()
{
#ifdef XF86_VM
   if (inVmMode)
   {
      videoMode->resetVideoMode();
      inVmMode=false;
   }
#endif

   /* change the popup menu */
   menu->setItemEnabled(menuItemFullScreen,true);

   /* Show all widgets  */
   parar->show();
   tocar->show();
   abrir->show();
   avanzar->show();
   retroceder->show();
   custom->show();
   configB->show();
   line->show();

   /* Restore background color */
   setBackgroundColor( saveBackground );

   inFullScreen = false;

   config->setGroup("scaling");
   if (config->readBoolEntry("grabmouse"))
      kapp->restoreOverrideCursor();

//   KWM::setDecoration( winId(), KWM::normalDecoration );

   QPoint p;
   p.setX(50);
   p.setY(50);
   recreate(NULL,0,p,true);//WStyle_Customize | WStyle_NormalBorder | WType_TopLevel, p, false);
}

void principal::executableChanged()
{
   if (video->isActive())
   {
      whatToDo=CHANGE_EXECUTABLE;
      video->stop();
   }
   else
   {
      whatToDo=NOTHING;
      fileName=video->getVideoFileName();
      if (!fileName.isEmpty())
         click_open();
   }
}

void principal::click_exit()
{
   this->close();
}

void principal::setCustomButton(int choice)
{
    switch (choice)
    {
       case 0:  custom->setPixmap(loader->loadIcon("ayuda.xpm"));
                QToolTip::remove(custom);
                QToolTip::add(custom,i18n("Help"));
                break;
       case 1:  custom->setPixmap(loader->loadIcon("exit.xpm"));
                QToolTip::remove(custom);
                QToolTip::add(custom,i18n("Exit"));
                break;
       case 2:  custom->setPixmap(loader->loadIcon("half.xpm"));
                QToolTip::remove(custom);
                QToolTip::add(custom,i18n("Half size"));
                break;
       case 3:  custom->setPixmap(loader->loadIcon("double.xpm"));
                QToolTip::remove(custom);
                QToolTip::add(custom,i18n("Double size"));
                break;
       case 4:  custom->setPixmap(loader->loadIcon("fullscreen.xpm"));
                QToolTip::remove(custom);
                QToolTip::add(custom,i18n("Full screen"));
                break;
       case 5:  custom->setPixmap(loader->loadIcon("idea.xpm"));
                QToolTip::remove(custom);
                QToolTip::add(custom,i18n("Video information"));
                break;
    }
}

void principal::click_custom()
{
   config->setGroup("interface");
   switch( config->readNumEntry("custombutton") )
   {
       case 0: click_ayuda(); break;
       case 1: click_exit(); break;
       case 2: click_half(); break;
       case 3: click_double(); break;
       case 4: goFullScreen(); break;
       case 5: click_info(); break;
   }
}




















