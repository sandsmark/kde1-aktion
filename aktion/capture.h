/***************************************************************************
                          capture.h  -  description
                             -------------------
    begin                : Sun Oct 17 1999
    copyright            : (C) 1999 by Guillermo P. Marotte
    email                : g-marotte@usa.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <qfileinfo.h>
#include <qpixmap.h>
#include <qwidget.h>
#include <kmsgbox.h>
/**
* Class for sinlge image capture from a widget
*
* @short Class for sinlge image capture from a widget.
* @author Guillermo P. Marotte (g-marotte@usa.net)
* @version 0.1
*/

class capture
{
private:
   int counter;
   QString baseName;
   QString extension;
   QString format;
   QString auxName;
   QString auxNum;
public:
   /**
   *  Capture a single image. Parameters: theWidget
   */
   void captureWidget(QWidget *);
   /**
   *  Set the parameters. Parameters: theDirectory, theFilename, theFormat
   */
   void setParameters(QString,QString,QString);
   /**
   *  Resets the file counter
   */
   void resetCounter();
};
