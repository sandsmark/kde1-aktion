#ifndef PRINCIPAL_H
#define PRINCIPAL_H

/* Project Specific Includes */
#include "aktionConf.h"
#include "kxanim.h"
#include "capture.h"
#include "aktionVm.h"
#include "logotipe.h"
#include "aktionprogress.h"

/* Library Includes */
#include <qcolor.h>
#include <qdialog.h>
#include <qevent.h>
#include <qfont.h>
#include <qkeycode.h>
#include <qlabel.h>
#include <qmsgbox.h>
#include <qpixmap.h>
#include <qpopmenu.h>
#include <qrect.h>
#include <qstring.h>
#include <qslider.h>
#include <qtimer.h>
#include <qtooltip.h>
#include <qwidget.h>
#include <kwm.h>
#include <kapp.h>
#include <ktopwidget.h>
#include <kfiledialog.h>
#include <kiconloader.h>
#include <kbutton.h>
#include <kconfig.h>
#include <kmsgbox.h>
#include <ktopwidget.h>
#include <drag.h>
#include <kurl.h>

#define NOTHING           0
#define CLOSE_THE_APP     1
#define NEW_VIDEO         2
#define CONTINUE_LOADING  3
#define CHANGE_EXECUTABLE 4

class principal : public KTopLevelWidget
{
	 Q_OBJECT
private:
	KButton *parar;
	KButton *tocar;
	KButton *abrir;
	KButton *avanzar;
	KButton *retroceder;
        KButton *custom;
        KButton *configB;
        QFrame *line;
        logotipe *logoWidget;
        KXAnim *video;
        KIconLoader *loader;
        QString lastDir;
        QString fileName;
        int whatToDo;
        int menuItemFullScreen;
        KConfig *config;
	KDNDDropZone *dropZone;
        void changeSize(float);
        void changeInitialSize();
        void dynamicResize();
        void selectMinimumSize();
        void setParameters();
        void continueLoading();
        void executableChanged();
        void toggleButtons(bool);
        void setCustomButton(int);
        bool inFullScreen;
        bool parametersChanged;
        bool isNewVideo;
        QPopupMenu *menu;
        QPopupMenu *initialMenu;
        QSlider *volumeSlider;
        bool externalChange;
        QTimer *timer;
        QColor saveBackground;
        QRect oldGeometry;
        capture theCapturer;
#ifdef XF86_VM
        aktionVm *videoMode;
        bool inVmMode;
#endif
			  /** Bar to reflect the video playing time  */
			  aktionProgress *timeBar;
        QTimer temporizador;
        Colormap *color;
			  QLabel *elapsedTime;
			  QLabel *totalTime;
public:
	principal( char *name=0);
        ~principal();
        void loadFile(const char *);
protected:
        virtual void closeEvent( QCloseEvent *);
        virtual void keyPressEvent( QKeyEvent *);
        virtual void resizeEvent( QResizeEvent *);
signals:
public slots:
        void click_open();
        void click_play();
        void click_stop();
        void click_forward();
        void click_backward();
        void click_ayuda();
        void click_custom();
        void click_config();
        void click_info();
        void click_original();
        void click_half();
        void click_double();
        void click_exit();
        void waitForKXanimExit();
        void receiveMouseClick( QMouseEvent *);
        void volumeSliderMoved(int);
        void maximize();
        void goFullScreen();
        void undoFullScreen();
        void checkStates();
        void fileDroped(KDNDDropZone *);
        void mostrarColormaps();
};
#endif





