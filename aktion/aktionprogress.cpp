/***************************************************************************
                          aktionprogress.cpp  -  description
                             -------------------
    begin                : Sun Jan 16 2000
    copyright            : (C) 2000 by Guillermo P. Marotte
    email                : g-marotte@usa.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "aktionprogress.h"
#include <iostream>

aktionProgress::aktionProgress(QWidget * parent=0, const char * name=0, WFlags f=0)
															: QProgressBar(1000,parent)
{
	connect(&timer,SIGNAL(timeout()),this,SLOT(update()));
	this->setStyle(MotifStyle);
	this->setFrameStyle( QFrame::NoFrame );
	totalLabel = 0L;
	fps=0;
	frames=0;
}

aktionProgress::~aktionProgress()
{
}

/**  */
void aktionProgress::play()
{
	timer.start(TIMER_INTERVAL);
}

/**  */
void aktionProgress::pause()
{
	timer.stop();
}

/**  */
void aktionProgress::stop()
{
	timer.stop();
	this->reset();
	if (totalLabel!=0L)
	{
		if (fps!=0)
			elapsedLabel->setText("0:00:00");
		else
			elapsedLabel->setText("0");
	}
}

/**  */
void aktionProgress::stepfw()
{
	timer.stop();
	update();
}

/**  */
void aktionProgress::stepbw()
{
	timer.stop();
	update();
}

void aktionProgress::setParameters(KXAnim *video_object, QLabel *e, QLabel *t)
{
	this->reset();
	video = video_object;
	frames = video->getVideoFrames();
	fps = video->getVideoSpeed();
	elapsedLabel = e;
	totalLabel = t;

	if (fps!=0)
	{
		int h,m,s;
		s = int(frames / fps);
		h = (s / 60) / 60;
		m = (s / 60) % 60;
		s = s % 60;
		aux.sprintf("%1d:%2d:%2d",h,m,s);
		aux.replace( QRegExp(" "), "0" );
		totalLabel->setText(aux);
	}
	else
	{
		aux.sprintf("%4d fr",frames);
		totalLabel->setText(aux);
	}
}

/** Update the frame counter */
void aktionProgress::update()
{
	long int frame=video->getCurrentFrame()+1;
	int newProgress=int(frame*1000/frames);

	if (fps!=0)
	{
		int h,m,s;
		s = int(frame / fps);
		h = (s / 60) / 60;
		m = (s / 60) % 60;
		s = s % 60;
		aux.sprintf("%1d:%2d:%2d",h,m,s);
		aux.replace( QRegExp(" "), "0" );
		elapsedLabel->setText(aux);
	}
	else
	{
		aux.sprintf("%4d",frame);
		elapsedLabel->setText(aux);
	}

	if (newProgress<this->progress())
		reset();
	this->setProgress(newProgress);
}

/** Show the time indicator */
bool aktionProgress::setIndicator( QString &indicator, int progress, int totalStep)
{
	indicator="";
	return true;
}














