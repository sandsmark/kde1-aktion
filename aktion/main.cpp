/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Sun Oct 17 1999
    copyright            : (C) 1999 by Guillermo P. Marotte
    email                : g-marotte@usa.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/* Main Include */
#include "main.h"

int main(int argc,char *argv[])
{
	KApplication app(argc,argv);
	principal mainWindow("topWidget");
	
//        app.setMainWidget(&mainWindow);
        mainWindow.show();
        
	if (argc > 1)
	   mainWindow.loadFile(argv[1]);

	return app.exec();
}
