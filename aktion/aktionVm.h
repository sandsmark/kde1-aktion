/***************************************************************************
                          aktionVm.h  -  description
                             -------------------
    begin                : Sun Oct 17 1999
    copyright            : (C) 1999 by Guillermo P. Marotte
    email                : g-marotte@usa.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef AKTIONVM_H
#define AKTIONVM_H

#include "config.h"

#ifdef XF86_VM

#define QT_CLEAN_NAMESPACE
#include <qwidget.h>
#undef QT_CLEAN_NAMESPACE

/**
* Class for VMode and DGA interaction
*
* @short Class for VMode and DGA interaction.
* @author Guillermo P. Marotte (g-marotte@usa.net)
* @version 0.1
*/

class aktionVm : public QWidget
{
private:
   int prevW;
   int prevH;
public:
   aktionVm(QWidget *);
   /**
   * This function tries to set the requested video mode
   * Parameters: width and height
   * Returns   : width and height of the closest video mode available
                 TRUE if the mode is available
                 FALSE if the mode is too big
   */
   bool setVideoMode(int*,int*);
   /**
   * Returns to the previous video mode.
   */
   void resetVideoMode();
};

#endif // XF86_VM
#endif // AKTIONVM_H

