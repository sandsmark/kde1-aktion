/***************************************************************************
                          logotipe.cpp  -  description
                             -------------------
    begin                : Sun Oct 17 1999
    copyright            : (C) 1999 by Guillermo P. Marotte
    email                : g-marotte@usa.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "logotipe.h"

logotipe::logotipe(QWidget *parent, const char *name) : QWidget( parent, name)
{
}

void logotipe::mousePressEvent( QMouseEvent *mouse)
{
   emit mouseClick(mouse);
}

