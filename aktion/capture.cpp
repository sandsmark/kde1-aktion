/***************************************************************************
                          capture.cpp  -  description
                             -------------------
    begin                : Sun Oct 17 1999
    copyright            : (C) 1999 by Guillermo P. Marotte
    email                : g-marotte@usa.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "capture.h"

void capture::captureWidget(QWidget *base)
{
   QPixmap *pixmap=new QPixmap(base->width(), base->height());

   bitBlt(pixmap,0,0,base,0,0,base->width(), base->height(),CopyROP);
   auxNum.setNum(counter);
   auxName=baseName+auxNum+extension;
   if(!pixmap->save(auxName,format))
   {
      QString s;
      s="Can't save image to file:/n";
      s+=auxName;
      KMsgBox::message(0L,"aktion!",s);
   }
   counter++;
   delete pixmap;
}

void capture::setParameters(QString dir, QString name, QString fmt)
{
   QFileInfo fi(name);

   baseName=dir;
   if (strcmp(baseName.right(1),"/")!=0)
      baseName+="/";
   baseName+=fi.baseName();
   format=fmt;
   extension="."+format.lower();
}

void capture::resetCounter()
{
   counter=0;
}

